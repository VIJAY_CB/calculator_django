from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .models import data
from .serializers import dataserializer
from .serializers import LoginSerializer
from django.contrib.auth import login as django_login,logout as django_logout

from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token



# Create your views here.
class calc(APIView):
    authentication_classes=[TokenAuthentication]
    permission_classes=[IsAuthenticated]
    def get(self,request):
    	num1=request.GET['num1']
    	num2=request.GET['num2']
    	if data.objects.filter(num1=num1,num2=num2,op='+'):
    		ans=data.objects.filter(num1=num1,num2=num2,op='+').values('res') 
    		return Response({'success':ans[0],"status":"200 OK"})
    	else:
	        ans=int(num1)+int(num2)             
	        result = data(num1=num1,num2=num2,res=ans,op="+")   
	        result.save()              
	        return Response({'Succesful ans is':ans,"status":"200 OK"})

    def put(self,request):
        num1=request.GET['num1']
        num2=request.GET['num2']
        if data.objects.filter(num1=num1,num2=num2,op='-'):
            ans=data.objects.filter(num1=num1,num2=num2,op='-').values('res') 
            return Response({'success':ans[0],"status":"200 OK"})
        else:
            ans=int(num1)-int(num2)             
            result = data(num1=num1,num2=num2,res=ans,op="-")   
            result.save()              
            return Response({'Succesful ans is':ans,"status":"200 OK"})    

    def post(self,request):
        num1=request.GET['num1']
        num2=request.GET['num2']
        if data.objects.filter(num1=num1,num2=num2,op='*'):
            ans=data.objects.filter(num1=num1,num2=num2,op='*').values('res') 
            return Response({'success':ans[0],"status":"200 OK"})
        else:
            ans=int(num1)*int(num2)             
            result = data(num1=num1,num2=num2,res=ans,op="*")   
            result.save()              
            return Response({'Succesful ans is':ans,"status":"200 OK"})

    def delete(self,request):
        num1=request.GET['num1']
        num2=request.GET['num2']
        if data.objects.filter(num1=num1,num2=num2,op='/'):
            ans=data.objects.filter(num1=num1,num2=num2,op='/').values('res') 
            return Response({'success':ans[0],"status":"200 OK"})
        else:
            ans=int(num1)/int(num2)             
            result = data(num1=num1,num2=num2,res=ans,op="/")   
            result.save()              
            return Response({'Succesful ans is':ans,"status":"200 OK"})        


class LoginView(APIView):
    def post(self,request):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        django_login(request,user)
        token , created = Token.objects.get_or_create(user=user)
        return Response({"token":token.key},status=200)

class LogoutView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    def post(self, request):
        request.user.auth_token.delete()
        django_logout(request)
        return Response("Logout Successful")

		

