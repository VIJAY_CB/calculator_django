from django.urls import path

from . import views

urlpatterns = [
    path('',views.calc.as_view()),
    path('login',views.LoginView.as_view()),
    path('logout',views.LogoutView.as_view()),
    #path('product/<int:pk>',views.product_detail),
]
