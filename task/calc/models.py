from django.db import models

# Create your models here.

class data(models.Model):
	num1 = models.IntegerField()
	num2 = models.IntegerField()
	res  = models.IntegerField()
	op   = models.CharField(max_length=10)

	def __str__(self):
		return self.op