from rest_framework import serializers
from .models import data

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from rest_framework import exceptions


class dataserializer(serializers.ModelSerializer):
	class Meta:
		model = data
		fields = ('num1','num2','op','res')


class LoginSerializer(serializers.Serializer):
	username = serializers.CharField()
	password = serializers.CharField()
	def validate(self,data):
		username = data.get("username","")	
		password = data.get("password","")
		if username and password:
				user=authenticate(username=username,password=password)
				if user:
					if user.is_active:
						data["user"]=user
					else:
						msg="User is deactivated"
						raise exceptions.ValidationError(msg)
				else:
					msg="Unable to login with given Credentials"
					raise exceptions.ValidationError(msg)			

		else:
				msg="Must give Username and password"
				raise exceptions.ValidationError(msg)
		return data	

